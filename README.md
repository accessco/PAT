# Personal Assistance Technology

A (Hopefully) extendable, Python Based Digital Assistant based off of Coqui's STT and TTS.

**THIS PROJECT IS INTENDED FOR USE ON LINUX ONLY!** You can attempt to get this working on windows, but **ZERO SUPPORT WILL BE PROVIDED**

# Features

None lmao

I should clarify, out of the box, no features will be provided. Instead, PAT uses a Module system. 
Modules are extensions to the base functionality of PAT. They can be installed by saying "Install (NAME) module"
