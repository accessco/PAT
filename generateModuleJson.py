import json
import os 
import urllib.request


with urllib.request.urlopen("https://gitlab.com/accessco/pat-module-repo/-/raw/main/repo.json") as url:
    s = url.read()

    data = json.loads(s)

    module = data["Beta"]["help"]

    os.system("cd PAT/Modules && git clone " + module["CloneADDR"])

    module["Repo"] = "Dev"

    moduleDict = {}
    moduleDict["help"] = module

    with open("PAT/Modules/installedModules.json", 'w') as json_file:
        json.dump(moduleDict, json_file, 
                    indent=4,  
                    separators=(',',': '))
