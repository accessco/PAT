conda env create -f PATEnv.yml

conda activate PAT
conda activate PAT && pip install -r requirements.txt
wget https://github.com/mozilla/DeepSpeech/releases/download/v0.9.3/deepspeech-0.9.3-models.pbmm
wget https://github.com/mozilla/DeepSpeech/releases/download/v0.9.3/deepspeech-0.9.3-models.scorer
wget https://websockets.readthedocs.io/en/stable/_downloads/c350abd2963d053f49c19e58cceced69/localhost.pem
mkdir PAT/Modules && touch PAT/Modules/installedModules.json
python generateModuleJson.py
touch initalized
conda activate PAT && python PAT/Scripts/liveSTT.py -m deepspeech-0.9.3-models.pbmm -s deepspeech-0.9.3-models.scorer & conda activate PAT && python PAT/firstTimeSetup.py


