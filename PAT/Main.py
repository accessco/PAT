import websockets
import asyncio
import functools
import threading
import TTS
import json
import ssl 
import urllib.request
import re
import os
# TTS Port = 1515
# Module Port = 1616
print("Debug: Main")

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
ssl_context.load_verify_locations("./localhost.pem")

async def AI(uri):
    async with websockets.connect(uri, ssl=ssl_context) as websocketSTT:
        await websocketSTT.recv()
        wakeWord = "pat" # Lowercase
        await websocketSTT.send('hey')
        async def commandHandler(command):
        # TODO: Code the rest of the funni stuff
            if "uninstall" in command.lower():
                moduleName = re.search(r'uninstall(.*?)module', command.lower()).group(1)
                moduleName = moduleName.strip()
                await uninstallModule(moduleName)
            elif "install" in command.lower():
                moduleName = re.search(r'install(.*?)module', command.lower()).group(1)
                command = command.lower()
                repoName = "Main"
                if "repo" in command.lower():
                    repoTempName = re.search(r'the(.*?)repo', command).group(1)
                    repoTempName = repoTempName.strip()
                    if repoTempName == "beta":
                        repoName = "Beta"
                    else:
                        repoName = "Dev"
                moduleName = moduleName.strip()
                print("ModuleName: ", moduleName)
                print("RepoName: ", repoName)
                await installModule(repoName, moduleName)
            # TODO: Code the rest of the funni stuff
            else:
                print("Debug")


        while True:
            detectedSentence = await websocketSTT.recv()
            print(detectedSentence) # Start receiving things, not only once
            if wakeWord in detectedSentence.lower():
                print("Wake Word Detected.")
                # TODO: Implement TTS, and Module Support
                detectedSentence = detectedSentence.lower()
                before_keyword, keyword, after_keyword = detectedSentence.partition(wakeWord)
                print("Debug, Before Keyword: '", before_keyword, "' length: ", len(before_keyword))
                print("Debug, After Keyword: '", after_keyword, "' length: ", len(after_keyword))
                if len(before_keyword) != 0:
                    print("Debug: length before keyword greater than 0")
                    await commandHandler(detectedSentence)
                elif len(after_keyword) != 0:
                    print("Debug: length after keyword greater than 0")
                    await commandHandler(detectedSentence)
                else:
                    print("Debug: Only the Keyword was registered.")
                    await patSay("I'm listening.")
                    detectedSentence = await websocketSTT.recv()
                    await commandHandler(detectedSentence)


            else:
                print("No wake word.")
                installModule("Main", "Ping")
            
    

async def ModuleHandler(websocketModule, path, moduleLoop, command, websocketSTT, moduleName):

    await websocketModule.send('security check')
    moduleResponse = await websocketModule.recv()
    if moduleResponse != moduleName:
        moduleLoop.get_event_loop().stop()
        patSay("Oops! The module security check failed. The connection could not go as planned.")
        return
    await websocketModule.send('check passed, go ahead')
    await websocketModule.send("The command was '" + command + "'")
    moduleConnectionActive = True
    while(moduleConnectionActive):
        moduleResponse = await websocketModule.recv()
        if moduleResponse == "say":
            await websocketModule.send('what should I say?')
            moduleResponse = await websocketModule.recv()
            await patSay(moduleResponse)
            await websocketModule.send('command completed. what else?')
        elif moduleResponse == "listen":
            await websocketModule.send('listening')
            userInput = await websocketSTT.recv()
            await websocketModule.send(userInput)
            await websocketModule.send('command completed. what else?')
        elif moduleResponse == "terminate":
            await websocketModule.send("terminating gracefully.")
            moduleLoop.get_event_loop().stop()
            return
        else:
            await websocketModule.send("Bad command, terminating ungracefully.")
            moduleLoop.get_event_loop().stop()
            patSay("Oops! Something went wrong with the commands sent by the module.")
            return


async def patSay(sentence):
    # TODO: Implement TTS
    # TTS Model = en/ljspeech/tacotron2-DDC_ph
    print("PAT: " + sentence)

def moduleInstalled(command):
    f = open('./PAT/Modules/installedModules.json')
    json.load(f)
    # TODO: Continue working on this feature, returns module name if found, returns null if not found 
    return

async def installModule(subRepo, moduleName):
    with urllib.request.urlopen("https://gitlab.com/accessco/pat-module-repo/-/raw/main/repo.json") as url:
        s = url.read()

        data = json.loads(s)
        print(data)
        # Thank you Stack Overflow
        # https://stackoverflow.com/questions/40827356/find-a-value-in-json-using-python
        try:
            module = data[subRepo][moduleName]
            print(module)
        except KeyError:
            print("Error, something went wrong while looking up the module.")
            patSay("Error, the module wasn't found in the repo. Please check that the module is in the repo you chose.")

        os.system("cd PAT/Modules && git clone " + module["CloneADDR"])

        module["Repo"] = subRepo

        with open("PAT/Modules/installedModules.json") as moduleRegistry:
            listObj = json.load(moduleRegistry)
        
        listObj[moduleName] = module

        with open("PAT/Modules/installedModules.json", 'w') as json_file:
            json.dump(listObj, json_file, 
                                indent=4,  
                                separators=(',',': '))
        await patSay("Module Installed.")

async def uninstallModule(moduleName):
    with open("PAT/Modules/installedModules.json") as moduleRegistry:
            data = json.load(moduleRegistry)

    try:
        module = data[moduleName]
        print(module)
    except KeyError:
        print("Error, module not found.")
    
    os.system("rm -r -f PAT/Modules/" + module["Dir"])

    del(data[moduleName])

    with open("PAT/Modules/installedModules.json", 'w') as json_file:
            json.dump(data, json_file, 
                                indent=4,  
                                separators=(',',': '))
    await patSay("Module uninstalled.")




def lookupModule(command):
    # TODO: Code the rest of this function.
    # Returns a dict of the module if found, returns "null" if false.
    with open("PAT/Modules/installedModules.json") as moduleRegistry:
            listObj = json.load(moduleRegistry)

    
def runModule(moduleDict):
    return




def Main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(AI('wss://localhost:1515'))
    loop.get_event_loop().run_forever()
    print("Test")
Main()

        


