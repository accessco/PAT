import websockets
import asyncio
import functools
import ssl
print("Debug: Emulated STT")
async def Main(websocket, path):
    await websocket.send('hey')
    await websocket.recv()
    while True:
        emulatedText = input("What do you want to say: ")
        await websocket.send(emulatedText)
        print("Message Emulated.")

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
ssl_context.load_cert_chain("./localhost.pem")


if __name__ == '__main__': 
    start_server = websockets.serve(Main, "localhost", 1515, ssl=ssl_context)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()